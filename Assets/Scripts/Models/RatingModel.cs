﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatingModel
{
    public string Source { get; set; }
    public string Value { get; set; }
}