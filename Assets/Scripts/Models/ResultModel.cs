﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ResultModel 
{
  public IList<ItemModel> Search { get; set; }
  public string TotalResults { get; set; }
  public string Response { get; set; }
}
