﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class ItemModel : MonoBehaviour
{
    public string Title { get; set; }
    public string Year { get; set; }
    public string ImdbID { get; set; }
    public string Type { get; set; }
    public string Poster { get; set; }

}
