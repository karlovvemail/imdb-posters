﻿using System.Collections;
using System;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.UI;

public abstract class AbstractUIController : MonoBehaviour
{  
    [SerializeField]
    protected TextMeshProUGUI title, imdbRate;
    [SerializeField]
    protected RawImage posterImage;

    protected CanvasGroup canvasGroup;
    protected IEnumerator DownloadImage(string mediaUrl)
    {   
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(mediaUrl);
        yield return request.SendWebRequest();
        if(request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
            posterImage.texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
    }
    
    protected CanvasGroup GetCanvas(String key) => GetComponentInParent<ScreenController>()
        .Screens.Single(x => x.Key == key)
        .Value
        .gameObject
        .GetComponent<CanvasGroup>();

    protected void DisableCanvas(CanvasGroup canvasGroup)
    {
        canvasGroup.DOFade(0f, 0.7f);
        SetCanvasStats(canvasGroup, false);

    }
    
    protected void EnableCanvas(CanvasGroup canvasGroup)
    {
        canvasGroup.DOFade(1f, 0.7f);
        SetCanvasStats(canvasGroup, true);
    }

    private void SetCanvasStats(CanvasGroup canvasGroup, bool state)
    {
        canvasGroup.interactable = state;
        canvasGroup.blocksRaycasts = state;
    }
    
    protected abstract void Enable();
    
}
