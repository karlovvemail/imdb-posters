﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScreenController : MonoBehaviour
{
    public Dictionary<String, GameObject> Screens  {get; set; }
    // Start is called before the first frame update
    void Start()
    {
        Screens = new Dictionary<string, GameObject>();
        for (int i = 0; i < transform.childCount; i++)
            Screens.Add( transform.GetChild(i).name, 
                transform.GetChild(i).gameObject);
    }
}
