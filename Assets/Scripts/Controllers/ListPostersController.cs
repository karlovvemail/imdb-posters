﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using TMPro;
using UniRx;
using UnityEngine.UI;
public class ListPostersController : MonoBehaviour
{
    [SerializeField]
    private  UIItemController posterItem;
    
    [SerializeField]
    private GridLayoutGroup grid;
    public TMP_InputField inputText;
    List<UIItemController> lastCard = new List<UIItemController>();
    void Start()
    {
        CancellationTokenSource cts = null;
            inputText.onValueChanged.AsObservable()
                .Subscribe(async _ =>
                {
                    if (_.Length < 2)
                        ClearCards();
                    if (cts != null)
                    {
                        cts.Cancel();
                        await ProcessDataAsync(_, cts.Token);
                        return;
                      
                    }   
                    cts = new CancellationTokenSource();
                    await ProcessDataAsync(_, cts.Token);
                });
    }
    
    
    private async Task ProcessDataAsync(String name, CancellationToken token)
    {
        try
        {
            token.ThrowIfCancellationRequested();
            InstantiateCard(await GetMovies(name));
            token.ThrowIfCancellationRequested();

        }
        catch (OperationCanceledException exception)
        {
            Debug.Log(exception.Message);
            InstantiateCard(await GetMovies(name));
        }
    }

    private void InstantiateCard(List<PosterModel> movies)
    {
        ClearCards();
        if (name.Length > 2)
            foreach (var posterModel in movies)
            {
                var newItem = Instantiate(posterItem).gameObject; 
                newItem.transform.SetParent(grid.transform);
                newItem.GetComponent<UIItemController>().ItemModel
                    = posterModel;
                lastCard.Add(newItem.GetComponent<UIItemController>());
            }
    }

    private void ClearCards()
    {
        if (lastCard.Count <= 0)
            return;
        foreach (var item in lastCard)
        {
            item.DeleteCards();
        }            
        lastCard.Clear();    
    }
    
    //get first 10 items of posters search
    async  Task<List<PosterModel>>  GetMovies(String name)
    {
        List<PosterModel> posterModels = new List<PosterModel>();
        var www = await new WWW("http://www.omdbapi.com/?s="+name+"&apikey=b99e3731");
        if (!string.IsNullOrEmpty(www.error) || www.text.Contains("False"))
        {
            GetNotification().OpenNotification();
            throw new Exception();
        }

        GetNotification().CloseNotification();
        foreach (var itemModel in Newtonsoft.Json.JsonConvert.DeserializeObject<ResultModel>(www.text).Search)
        {
            var poster = await GetPoster(itemModel.ImdbID);
            posterModels.Add(poster);
        }
        
        posterModels.Sort(new NumberTextComparer());
        posterModels.Reverse();
        return posterModels ;
    }
        
    //get poster by id
    async Task<PosterModel>  GetPoster(String imdbId)
    {
        var www = await new WWW("http://www.omdbapi.com/?i="+imdbId+"&apikey=b99e3731&plot=full");
        if (!string.IsNullOrEmpty(www.error) || www.text.Contains("False"))
        {
            GetNotification().OpenNotification();
            throw new Exception();
        }
        return Newtonsoft.Json.JsonConvert.DeserializeObject<PosterModel>(www.text);
    }
    
    public static Version TryGetVersion( string item)
    {
        Version ver;
        bool success = Version.TryParse(item, out ver);
        if (success) return ver;
        return null;
    }
    
    private NotificationManager GetNotification() => GetComponentInParent<ScreenController>()
        .Screens.Single(x => x.Key == "Error")
        .Value
        .gameObject
        .GetComponent<NotificationManager>();
 
}




