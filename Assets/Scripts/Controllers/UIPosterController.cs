﻿using System.Linq;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class UIPosterController : AbstractUIController
{
    [SerializeField] private TextMeshProUGUI  year, director, actors, plot;
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    protected override  void Enable()
    {
        EnableCanvas(canvasGroup);
    }
    
    public void Open(PosterModel posterModel)
    {
        Enable();
        SetValues(posterModel);
    }
    
    public void ClosePoster()
    {
        DisableCanvas(canvasGroup);
        
        var keyValuePair = GetCanvas("MoviesList");
        EnableCanvas(keyValuePair);
        keyValuePair.DOFade(1, 0.5f);
        
        keyValuePair = GetCanvas("Search");
        EnableCanvas(keyValuePair);
    }
    private void SetValues(PosterModel posterModel)
    {
        title.text = posterModel.Title;
        imdbRate.text = posterModel.imdbRating;
        year.text = posterModel.Year;
        director.text = posterModel.Director;
        actors.text = posterModel.Actors;
        plot.text = posterModel.Plot;
        StartCoroutine(DownloadImage(posterModel.Poster));
    }
}
