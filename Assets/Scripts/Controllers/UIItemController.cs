﻿using System.Linq;
using UnityEngine;
using DG.Tweening;
using UniRx;
using System;
using System.Collections;

public class UIItemController : AbstractUIController
{
    public PosterModel ItemModel { get; set; }
    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        Enable();
        this.transform.ObserveEveryValueChanged(x => 
            ItemModel).Subscribe(x =>
        {
            ItemModel = x;
            title.text = x.Title;
            imdbRate.text = x.imdbRating;
            StartCoroutine(DownloadImage(x.Poster));
        });
    }

    protected override  void Enable()
    {
        EnableCanvas(canvasGroup);
    }

    public void DeleteCards()
    {
        canvasGroup.DOFade(0f, 0.5f).OnComplete(() => Destroy(gameObject));
    }

    public void ClickViewMore()
    {
        var keyValuePair = GetCanvas("MoviesList");
        keyValuePair.DOFade(0f, 0.5f).OnComplete(()=>OpenPoster());
        DisableCanvas(keyValuePair);
        keyValuePair = GetCanvas("Search");
        DisableCanvas(keyValuePair);
    }
    private void OpenPoster()
    {
        var poster =  FindObjectOfType<UIPosterController>();
        poster.Open(ItemModel);
    }
}
